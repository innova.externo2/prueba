import { getFirstDay } from '@/utils/calculateTime.js'

const padNmb = (nStr, nLen) => {
    const sRes = String(nStr)
    const sCeros = "0000000000"
    return sCeros.substr(0, nLen - sRes.length) + sRes
}

const secondsToTime = (secs) => {
    const hor = Math.floor(secs / 3600)
    const min = Math.floor((secs - hor * 3600) / 60)
    const sec = secs - hor * 3600 - min * 60
    return (
        padNmb(hor, 2) +
        ":" +
        padNmb(min, 2) +
        ":" +
        padNmb(sec, 2)
    )
}

const stringToSeconds = (time) =>{
    const sep1 = time.indexOf(":")
    const sep2 = time.lastIndexOf(":")
    const hor = time.substr(0, sep1)
    const min = time.substr(sep1 + 1, sep2 - sep1 - 1)
    return Number(min) * 60 + Number(hor) * 3600
}

const getTime = (firstTime, secondTime, operator) => {
    const secs1 = stringToSeconds(firstTime)
    const secs2 = stringToSeconds(secondTime)
    let secsDif
    if(operator == 'sum') {
        secsDif = secs1 + secs2
    } else {
        if(secs1 > secs2) {
            secsDif = secs1 - secs2
        } else {
            secsDif = secs2 - secs1
        }
    }
    return secondsToTime(secsDif)
}


const recordArray = (firstTime, secondTime, operator, record) => {
    const arr = [firstTime]
    for (let i = 0; i < record; i++) {
        arr.push(getTime(arr[i], secondTime, operator));
    }
    return arr
}

const addingZerosToTheDay = (day) => {
    if(String(day).length < 2){
        day = '0'+ day
    }
    return day
}

const convertToMinutes = (hours, minutes) => {
    const result = (hours * 60) + minutes
    return result
}

const conditionalDate = (val) => {
    let getTime = val.split(":")
    let getHour = String(getTime[0])
    let hour = calculateHour(getHour)
    let getMin = String(getTime[1])
    let min = calculateHour(getMin)
    let conditionalHour = hour === '00' ? '24' : hour
    let time = `${conditionalHour}:${min}:00`
    return time
}

const calculateScheduleHours = (scheduleStartHour, scheduleStartMin) => {
    return calculateSchedule(scheduleStartHour, scheduleStartMin)
}

const durationProgramTitle = (val) => {
    const data = calculateTime(val)
    let time
    if(data <= '00:05:00') {
        time = 'letters-small font-size-11 font-weight-0'
    } else if (data > '00:05:00' && data <= '00:15:00') {
        time = 'font-size-14 p-absolute t-0 font-weight-0'
    } else if(data > '00:15:00' && data <= '00:30:00') {
        time = 'font-size-14 font-weight-0'
    }  else if(data > '00:30:00' && data <= '01:00:00') {
        time = 'font-size-18'
    } else {
        time = 'letters-big'
    }
    return time
}

const durationProgramHeader = (val) => {
    const data = calculateTime(val)
    let time
    if(data <= '00:15:00') {
        time = 'letters-small'
    } else if (data > '00:15:00' && data <= '00:45:00') {
        time = 'letters-middle'
    } else {
        time = 'letters-normal'
    }
    return time
}

const durationProgramTitleLargometraje = (val) => {
    const {programTitle} = val
    const data = calculateTime(val)
    let time
    if(data <= '00:30:00') {
        time = sizeLetttersAboutProgram(programTitle, 'font-size-8', 'size-10')
    }  else if(data > '00:30:00' && data <= '01:00:00') {
        time = sizeLetttersAboutProgram(programTitle, 'size-10', 'font-size-11')
    } else {
        time = 'letters-middle'
    }
    return time
}

const isHaveClassification = (val) => {
    let value
    if(val) {
        value = `(${val})`
    } else {
        value = ''
    }
    return value
}

const isHaveActors = (val) => {
    let value, arr
    if(val && val.artisticInfo) {
        let exitsActors = val.artisticInfo.length > 0 ? val.artisticInfo[0].mainActors : []
        let counterActors = exitsActors.length
        if(counterActors > 2) {
            arr = [exitsActors[0], exitsActors[1]]
        } else {
            arr = exitsActors
        }
        value = arr
    }
    return value
}

const sortByHour = (programs) => {
    return programs.slice().sort(function (a, b){
        if(a.programPrincipal && b.programPrincipal) {
            //SE ORDENA POR TOTAL DE MINUTOS
            const firstProgramMinutesTotal = (a.programPrincipal.scheduleStartHour * 60) + a.programPrincipal.scheduleStartMin
            const secondProgramMinutesTotal = (b.programPrincipal.scheduleStartHour * 60) + b.programPrincipal.scheduleStartMin
            return firstProgramMinutesTotal - secondProgramMinutesTotal;
        }
    });
}

const dayStartAndDayEnd = (val) => {
    if(val && val.length > 1) {
        const counter = val.length
        const connect = val[0] == 'Lunes' ? 'a' : 'y'
        const counterCondicional = connect == 'a' ? 5 : 2
        const text = `${val[0]} ${connect} ${val[counter-1]}`
        return {
            text,
            value: counterCondicional
        }
    } else if(val && val.length == 1) {
        return {
            text: val[0],
            value: 1
        }
    }
}

const sizeLetttersAboutProgram = (programTitle, styleFirst, styleSecond) => {
    let size = String(programTitle.length)
    let value
    switch(true) {
        case size >= 25:
            value = `${styleFirst}`
            break
        default:
            value = `${styleSecond}`
    }
    return value
}

const calculateSchedule = (hour, min) => {
    let time = ''
    if(hour && min) {
        time = `${hour}:${min}`
    }
    return time
}

const calculateTime = (val) => {
    const {scheduleStartHour, scheduleStartMin, scheduleEndHour, scheduleEndMin} = val
    if(scheduleStartHour && scheduleStartMin && scheduleEndHour && scheduleEndMin) {
        const dateStart = calculateSchedule(scheduleStartHour, scheduleStartMin)
        const dateEnd = calculateSchedule(scheduleEndHour, scheduleEndMin)
        const data = getTime(conditionalDate(dateEnd), conditionalDate(dateStart), 'min')
        return data
    }
}

//convert format '20/12/2019 10:00:00'
const convertHourDate = (date, hours, minutes) => {
    const d = new Date(date).toLocaleDateString('en-GB')
    let newString = d + ' ' + hours + ':' + minutes + ':' + '00'
    return newString 
}

function sumaFecha (d, fecha) {
  fecha= new Date(fecha);
  fecha.setDate(fecha.getDate()+parseInt(d));
  var anno=fecha.getFullYear();
  var mes= fecha.getMonth()+1;
  var dia= fecha.getDate();
  mes = (mes < 10) ? ("0" + mes) : mes;
  dia = (dia < 10) ? ("0" + dia) : dia;
  var fechaFinal = dia+'/'+mes+'/'+anno;
  return (fechaFinal);
 }


const convertFormatDayForCrusses = ( format, programDate, conditional ) => {
  let newFormat = format.split(' ')
  let fech 
  if (newFormat[1] <= '05:16:00' && newFormat[1] >= '00:00:00'  ) {
    if (conditional == 'start' && newFormat[1] == '05:16:00' ) {
        fech = newFormat[0]
    }
    else {
      fech = sumaFecha(1, programDate )
    }
  }
  else {
    fech = newFormat[0]
  }
  let fecha = fech.split('/')

  let dia = fecha[0]
  let mes = fecha[1]
  let anio = fecha[2]

  let hora = mes + '/' + dia + '/' + anio  + ' ' + newFormat[1]
  let h = new Date(hora)
  return h
}

const calculateHour = (hour) => {
  if(hour) {
    return hour.length == 2 ? hour : `0${hour}`
  }
}

//FUNCION QUE RESTA 2 HORAS EN FORMATO FECHA Y RETORNA LAS HORAS EN STRING
const restHoursFormatDay = (val, programDate) => {
  const {scheduleStartHour, scheduleStartMin, scheduleEndHour, scheduleEndMin } = val
			
  let timeStartProgramCardsData = convertFormatDayForCrusses( convertHourDate(programDate, scheduleStartHour, scheduleStartMin), programDate, 'start' )
  let timeEndProgramCardsData = convertFormatDayForCrusses( convertHourDate(programDate, scheduleEndHour, scheduleEndMin), programDate, 'end' )
  
  let timeStart = new Date(String(timeStartProgramCardsData)).getTime()
  let timeEnd = new Date(String(timeEndProgramCardsData)).getTime()
  let hourDiff = timeEnd - timeStart
  let secDiff = hourDiff / 1000
  let minDiff = hourDiff / 60 / 1000
  let hDiff = hourDiff / 3600 / 1000
  let result = {}
  result.hours = Math.floor(hDiff)
  result.minutes = minDiff - 60 * result.hours
  
  let response =  `${calculateHour(String(result.hours))}:${calculateHour(String(result.minutes))}:00`
  return response

}



const arraySignalCode =  (val, signals)  => {
    let arrayIndex = []
    for(var x=0; x < val.length; x++){
      for(var y=0; y < signals.length; y++){
        if(signals[y] == val[x]){
          arrayIndex.push(y)
        }
      }
    }
    return arrayIndex
}

const verificatedCutsEnd = (val, time) => {
    let verificatedCuts = []
    if(val && val.length >= 1) {
      val.forEach((e, i) => {
        if(e.end <= time) {
          verificatedCuts.push(e)
        }
      })
    }
    return verificatedCuts
}

const calculateRestTimeForCuts = (time, cuts) => {
    let value = []
    let constDuration = cuts.map((e) => (e.duration))
    
    constDuration.forEach((e, i, arr) => {
      if(i == 0) {
        value.push(getTime(e, arr[i +1], 'sum'))
      } else {
        if(arr[i +1]) {
          value.push(getTime(value[i-1], arr[i +1], 'sum'))
        }
      }
    })
    let timeForRest = value.pop()
    let newTimeCalculated = getTime(time, timeForRest)
    return newTimeCalculated
}

const getWeek = (months)  =>{
    let dateSearch = new Date()
      
    let firstDayOfWeek, startDate, firstDay, secondDay, thirdDay, fourthDay, fifthDay, sixthDay, lastDay, endDate, month, month2, year, year2, dateString;

    if (dateSearch === '' || dateSearch === null) { 
    firstDayOfWeek = getFirstDay(new Date())
    startDate = getFirstDay(new Date())
    firstDay = firstDayOfWeek.getDate();
    secondDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)).getDate()
    thirdDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*2).getDate()
    fourthDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*3).getDate()
    fifthDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*4).getDate()
    sixthDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*5).getDate()
    lastDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*6).getDate()
    endDate = new Date(startDate.setDate(startDate.getDate() + 6))  
    month = months[firstDayOfWeek.getMonth()];
    month2 = months[endDate.getMonth()];
    year = firstDayOfWeek.getFullYear();
    year2 = endDate.getFullYear();
    } else {
    firstDayOfWeek = getFirstDay(new Date(dateSearch));
    startDate = getFirstDay(new Date(dateSearch))
    firstDay = firstDayOfWeek.getDate();
    secondDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)).getDate()
    thirdDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*2).getDate()
    fourthDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*3).getDate()
    fifthDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*4).getDate()
    sixthDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*5).getDate()
    lastDay = new Date(firstDayOfWeek.getTime() + (24*60*60*1000)*6).getDate()
    endDate = new Date(startDate.setDate(startDate.getDate() + 6))
    month = months[firstDayOfWeek.getMonth()];
    month2 = months[endDate.getMonth()];
    year = firstDayOfWeek.getFullYear();
    year2 = endDate.getFullYear();
    }
    
    if (month == month2 && year == year2  ) {
    dateString = `Semana del ${firstDay} al ${lastDay} de ${month} del ${year}`
    }
    else if (month !== month2 && year == year2 ) {
    dateString = `Semana del ${firstDay} de ${month} al ${lastDay} de ${month2} del ${year}`
    }
    else if (month !== month2 && year !== year2) {
    dateString = `Semana del ${firstDay} de ${month} del ${year} al ${lastDay} de ${month2} del ${year2}`
    }
    return dateString
}

// input 02/12/2019 dia/mes/año  ---- ouput 2019/12/02 año/mes/dia
const changePositionFormatDayString = (date) => {
  let newFormat = date.split('/')
  let format = newFormat[2] + '/' + newFormat[1] + '/' + newFormat[0] 
  return format
}

export { changePositionFormatDayString, restHoursFormatDay, convertFormatDayForCrusses, padNmb, secondsToTime, stringToSeconds, getTime, recordArray, addingZerosToTheDay, convertToMinutes, calculateHour, conditionalDate, calculateScheduleHours, calculateTime, durationProgramTitle, durationProgramHeader, durationProgramTitleLargometraje, isHaveClassification, isHaveActors, sortByHour, dayStartAndDayEnd, sizeLetttersAboutProgram, calculateSchedule, convertHourDate, arraySignalCode, verificatedCutsEnd, calculateRestTimeForCuts, getWeek}