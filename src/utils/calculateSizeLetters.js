const sizeLetttersAboutProgram = (programTitle, styleFirst, styleSecond) => {
    let size = String(programTitle.length)
    let value
    switch(true) {
        case size >= 22:
            value = `${styleFirst}`
            break
        default:
            value = `${styleSecond}`
    }
    return value
}

const calculateSchedule = (hour, min) => {
    let time = ''
    if(hour && min) {
        time = `${hour}:${min}`
    }
    return time
}

const sizeLetttersAboutProgramCompare = (programTitle, styleFirst, styleSecond, comparador) => {
    let size = String(programTitle.length)
    let value
    switch(true) {
        case size >= comparador:
            value = `${styleFirst}`
            break
        default:
            value = `${styleSecond}`
    }
    return value
}

export {sizeLetttersAboutProgram, calculateSchedule, sizeLetttersAboutProgramCompare}