var modifyText = function (txt) {
  // console.log(txt,'txt')
  if(txt !== undefined && txt !== null){
    txt = txt.toLowerCase()
    txt = txt.replace(/á/gi, 'a')
    txt = txt.replace(/é/gi, 'e')
    txt = txt.replace(/í/gi, 'i')
    txt = txt.replace(/ó/gi, 'o')
    txt = txt.replace(/ú/gi, 'u')
  } else {
    txt = ''
  }
  return txt
}
var modifyNumber = function (txt) {
  if(txt == undefined){
    txt = ''
  }
  return txt
}

var isNumber = function (evt) {
  evt = (evt) ? evt : window.event
  var charCode = (evt.which) ? evt.which : evt.keyCode
  if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
    evt.preventDefault()
  } else {
    return true
  }
}

export {modifyText, isNumber, modifyNumber}
