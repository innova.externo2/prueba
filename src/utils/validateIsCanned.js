const isCanned = (materialType) => { 
    const LIST_TYPE = ["Televisa", "Enlatado"]  
    const isIncludeType = LIST_TYPE.some(item=> materialType.indexOf(item) >= 0) 
    return isIncludeType 
} 

export { isCanned } 