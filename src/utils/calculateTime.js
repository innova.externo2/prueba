function padNmb (nStr, nLen) {
  var sRes = String(nStr)
  var sCeros = '0000000000'
  return sCeros.substr(0, nLen - sRes.length) + sRes
}

function stringToSeconds (tiempo) {
  var sep1 = tiempo.indexOf(':')
  var sep2 = tiempo.lastIndexOf(':')
  var hor = tiempo.substr(0, sep1)
  var min = tiempo.substr(sep1 + 1, sep2 - sep1 - 1)
  var sec = tiempo.substr(sep2 + 1)
  return (Number(sec) + (Number(min) * 60) + (Number(hor) * 3600))
}

function secondsToTime (secs) {
  var hor = Math.floor(secs / 3600)
  var min = Math.floor((secs - (hor * 3600)) / 60)
  var sec = secs - (hor * 3600) - (min * 60)
  return padNmb(hor, 2) + ':' + padNmb(min, 2) + ':' + padNmb(sec, 2)
}

function addTimes (t1, t2) {
  var secs1 = stringToSeconds(t1)
  var secs2 = stringToSeconds(t2)
  var secsDif = secs1 + secs2
  return secondsToTime(secsDif)
}

function restTimes (t1, t2) {
  var secs1 = stringToSeconds(t1)
  var secs2 = stringToSeconds(t2)
  var secsDif = secs1 - secs2
  return secondsToTime(secsDif)
}

function diferenciaTiempos(tiempoInicio,tiempoFin){
  let inicio = calcularTiempo(tiempoInicio);
  let fin = calcularTiempo(tiempoFin);
  if ( inicio > fin ) fin = fin + 24 * 60 * 60
  let diferencia = fin - inicio;
  return obtenerTiempoString(diferencia);
}
function calcularTiempo(tiempo){
  let tiempos = tiempo.split(':');
  let horas = parseInt(tiempos[0]) * 60 * 60;
  let minutos = parseInt(tiempos[1]) * 60;
  let segundos = parseInt(tiempos[2]);
  return horas + minutos + segundos;
}
function obtenerTiempoString(segundos){
  let hora = Math.trunc( segundos / 3600 );
  segundos = segundos % 3600;
  let minutos = Math.trunc(segundos / 60);
  segundos = segundos % 60;
  hora = hora<10? '0' + hora: hora;
  minutos = minutos<10? '0' + minutos:minutos;
  segundos = segundos<10? '0' + segundos:segundos;
  return `${hora}:${minutos}:${segundos}`
}

function getFirstDay (date) {
  let day = date.getDay() || 7
  if (day !== 1) {
    date.setHours(-24 * (day - 1))
  }
  return date
}

function convertDate (date) {
  let arrivalDate = new Date(date)
  let savedate = arrivalDate.toLocaleDateString('en-GB')
  return savedate
}

function validateTime(val, item) {
  return Number(val) > item ? '' : val
}

function formatDateString (date) {
  let nDate = new Date(date);
  let year = nDate.getFullYear();
  let month = nDate.getMonth()+1;
  let dt = nDate.getDate();
  if (dt < 10) {
    dt = '0' + String(dt) ;
  }
  if (month < 10) {
    month = '0' + String(month) ;
  }
  return String(dt) + "/" + String(month)  + "/" +  String(year) 
}

function convertNumberFormatUsd (numero ) {
    const opciones = {
      numeroDeDecimales: 2,
      separadorDecimal: ".",
      separadorMiles: ",",
      simbolo: " USD",
      posicionSimbolo: "d", 
    }

    const CIFRAS_MILES = 3;
    let numeroComoCadena = numero.toFixed(opciones.numeroDeDecimales);
    let posicionDelSeparador = numeroComoCadena.indexOf(opciones.separadorDecimal);
    if (posicionDelSeparador === -1) posicionDelSeparador = numeroComoCadena.length;
    let formateadoSinDecimales = "", indice = posicionDelSeparador;
    while (indice >= 0) {
      let limiteInferior = indice - CIFRAS_MILES;
      formateadoSinDecimales = (limiteInferior > 0 ? opciones.separadorMiles : "") + numeroComoCadena.substring(limiteInferior, indice) + formateadoSinDecimales;
      indice -= CIFRAS_MILES;
    }
    let formateadoSinSimbolo = `${formateadoSinDecimales}${numeroComoCadena.substr(posicionDelSeparador, opciones.numeroDeDecimales + 1)}`;
    return opciones.posicionSimbolo === "i" ?  formateadoSinSimbolo  : formateadoSinSimbolo ;
}

function convertFirstWordTouperCase  (string)  {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


export {addTimes, restTimes, getFirstDay, convertDate, validateTime, secondsToTime, calcularTiempo, obtenerTiempoString,
  diferenciaTiempos, formatDateString, convertNumberFormatUsd, convertFirstWordTouperCase }
