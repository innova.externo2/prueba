const scrollToTop = () => {
    window.scrollTo(0,0)
}
const textCapitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}
const capitalizeName = (array) => {
    return array.map((e) => ({name: textCapitalize(e.name)}))
}
const modelProgramByDay = (program) => {
    const programByDay = {
        chapters: program.chapters,
        dailyCuttingProgram: program.dailyCuttingProgram,
        programGridTitle: program.programGridTitle,
        geminisCodeProgram: program.geminisCodeProgram,
        programTitle: program.programGridTitle,
        dailyProgramDate: program.dailyProgramDate,
        completeDate: program.completeDate,
        programDate: program.programDate,
        scheduleStartHour: program.scheduleStartHour,
        scheduleStartMin: program.scheduleStartMin,
        scheduleEndHour: program.scheduleEndHour,
        scheduleEndMin: program.scheduleEndMin,
        timeStart: program.timeStart,
        timeEnd: program.timeEnd,
        materialTitle: program.materialTitle,
        codMaterial: program.codMaterial,
        firstActor: program.firstActor,
        secondActor: program.secondActor,
        actorsPreliminary: program.actorsPreliminary,
        historyTime: program.historyTime,
        materialExisting: program.materialExisting,
        materialHasChapter: program.materialHasChapter,
        materialType: program.materialType,
        typeOfContent: program.typeOfContent,
        refProgram: program.refProgram,
        isShowTemporadas: program.isShowTemporadas || false,
        isTemporada: program.isTemporada || false,
        IsCap: program.IsCap || false,
        EstTemp: program.EstTemp || false,
        Estado: program.Estado,
        bgColorByProgram: program.bgColorByProgram == undefined ? 'inherit' : program.bgColorByProgram,
        chapterColor: program.chapterColor,
        priceChapterColor: program.priceChapterColor,
        priceChapter: program.priceChapter || false,
        pricePassChapter: program.pricePassChapter || false,
        // priceChapter: program.priceChapter ? Boolean(program.priceChapter) : false,
        // pricePassChapter: program.pricePassChapter ? Boolean(program.pricePassChapter) : false,
        pricePassChapterColor: program.pricePassChapterColor,
        seasonStateColor: program.seasonStateColor,
        stateChapterColor: program.stateChapterColor, 
        nota: program.nota,
        isClasification: program.isClasification || false,
        clasificationColor: program.clasificationColor, 
        isUnitPrice: program.isUnitPrice || false,
        unitPriceColor: program.unitPriceColor,
        codProviderColor: program.codProviderColor,
        IsPricePass: program.IsPricePass || false,
        IsCodProvider: program.IsCodProvider || false,
        pricePassColor: program.pricePassColor, 
        generalNote: program.generalNote,
        rerunChapterColor: program.rerunChapterColor,
        providerCode: program.providerCode,
        unitPrice: program.unitPrice,
        season: program.season,
        userName: program.userName,
        classification: program.classification,
        codeFeatureFilm: program.codeFeatureFilm,
        theyAreVoiceActors: program.theyAreVoiceActors,
        idMaterialRef:  program.idMaterialRef,
        noteDay: program.noteDay,
        generalNoteBoss: program.generalNoteBoss,
        material: program.material,
        seasonColor: program.seasonColor
    }
    return programByDay
}

const daysSpecific = (data) => {
    let days = data.map((e) => {
      let response = e.completeDate.split(' ')
      return response[0]
    })
    // console.log(days, 'daysSpecific')
    return days
}

const setChooseDefaultDaySelect = (data) =>{
    if(data.length == 1) {
        return data
    } else {
        return []
    }
}

const isLargometraje = (value) => {
    if(value == 'Largometrajes' || value == 'LARGOMETRAJES') {
        return true
    } else {
        return false
    }
}


const revertFormatCurrency = (val) => {
    let value = val && typeof(val) == 'string' ? parseFloat(val.replace(/[^0-9\.]/g, '')) : val
    return value
  };
  
  const formatCurrency = (val) => {
      if(val) {
          let prevNumber = typeof(val) == 'string' ? revertFormatCurrency(val) : val;
          let number = prevNumber.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
          return number;
      } else {
          return val;
      }
  };


export {revertFormatCurrency,formatCurrency, scrollToTop, textCapitalize, capitalizeName, modelProgramByDay, daysSpecific, setChooseDefaultDaySelect, isLargometraje}