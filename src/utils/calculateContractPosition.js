const setGroupMat = (arr) => {
    let arrTypeContent = arr.map((e) => (e.typeOfContent))
    let valuesRepeat = {};
    arrTypeContent.forEach(function(i) { valuesRepeat[i] = (valuesRepeat[i]||0) + 1;})
    return  valuesRepeat
}

const agroupByContent = (arr) => {
    let agrupando = {};
    arr.forEach((obj, i) => {
      let cuts = obj.typeOfContent;
      let key = cuts;

      if (agrupando[key] == null) {
        agrupando[key] = [];
      }
      agrupando[key].push(obj);
    })
    return agrupando
}

const getPosition = (obj, refMaterials) => {
    const {typeOfContent, codMaterial} = obj
    let newArr = agroupByContent(refMaterials)
    let response
    if(newArr[typeOfContent] && newArr[typeOfContent].length >0) {
      newArr[typeOfContent].forEach((e, i) => {
        if(e.codMaterial  === codMaterial) {
          response = i+1
        }
      })
    }
    return response
}

export {setGroupMat, getPosition, agroupByContent}