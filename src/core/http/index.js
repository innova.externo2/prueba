// import axios from 'axios'
import {HTTP} from '../../main.js'

export default {
  getData: function (cb, dataType) {
    HTTP.get(`${dataType}`)
    .then(function (res) {
      cb( dataType, res) 
    })
  },

  getDataFilters: function (cb, payload) {
    HTTP.get(`${payload.type}/${payload.typeFilter}/${payload.typeMaterial}/typeOfContent/${payload.typeOfContent}/${payload.typeFilter2}`)
    .then(function (res) {
      cb( payload.typeOfContent, res) 
    })
  },

  patchDataFilter: async function (cb, payload, statusMutation) {
    HTTP.patch(`${payload.type}/${payload.id}`, payload.mat)
      .then(res => {
        this.getDataFilters(cb, payload)   
      })
  },
  changeAvailability: function (cb, payload, statusMutation) {
    let targetId = payload.id
    /* Info Log */
    let userName = localStorage.getItem('userName')
    let userEmail = localStorage.getItem('userEmail')

    let item = payload.type.slice(0, payload.type.length - 1)
    item = item.charAt(0).toUpperCase() + item.slice(1).toLowerCase()

    let valCod = 'cod' + item

    if (payload.payloadInfo.status) {
      HTTP.patch(`${payload.type}/${targetId}`, [
        { propName: 'status', value: false },
        { propName: 'userName', value: userName},
        { propName: 'userEmail', value: userEmail},
        { propName: valCod, value: payload.payloadInfo[valCod]}
      ])
        .then(function () {
          statusMutation = 'STATUS_FALSE'
          cb(statusMutation, payload)
        })
    } else {
      HTTP.patch(`${payload.type}/${targetId}`, [
        { propName: 'status', value: true },
        { propName: 'userName', value: userName},
        { propName: 'userEmail', value: userEmail},
        { propName: valCod, value: payload.payloadInfo[valCod]}
      ])
        .then(function () {
          statusMutation = 'STATUS_TRUE'
          cb(statusMutation, payload)
        })
    }
  },
  individualData: async function (cb, payload, statusMutation) {
    // const url = `https://griddata.herokuapp.com/${payload.type}/${payload.id}`
    await HTTP.get(`${payload.type}/${payload.id}`)
      .then(function (res) {
        let data
        if (payload.type === 'materials') {
          data = res.data.material
          statusMutation = 'material'
          cb(statusMutation, data)
        } else if (payload.type === 'programs') {
          data = res.data.program
          statusMutation = 'program'
          cb(statusMutation, data)
        } else if (payload.type === 'segments') {
          data = res.data.segment
          statusMutation = 'segment'
          cb(statusMutation, data)
        }
      })
  },
  updateData: async function (cb, payload, statusMutation) {
    await HTTP.patch(`${payload.key}/${payload.id}`, payload.mat)
      .then(res => {
        console.log('uD', res);
        if (res) {
          if(payload.key == 'materials' ||  payload.key == 'contracts') {
            this.getData(cb, payload.key)
          }
  
          if ( payload.key == 'materials' ) {
             statusMutation = 'PATCH_MATERIAL'
             cb(statusMutation, payload.data)
          }
        }
      })
  },
  weeklySchedule: async function (cb, payload, statusMutation) {
    
    HTTP.get(`grid/${payload.dateString}/${payload.programTitle}`)
      .then(function (res) {
        
        let data = res.data
        statusMutation = 'SET_GRID_PROGRAM'
        cb(statusMutation, data)
      })
  }
}
export const getInfo = async (dataType, route) => {
  try {
    const response = await HTTP.get(route)
    // console.log(response, 'response getInfo store')
    cb( dataType, response)
  } catch(err) {
    // console.log(err, 'getInfo store actions')
  }
}