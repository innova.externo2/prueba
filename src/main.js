import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/index'
import './core/services/registerServiceWorker'
import VueLocalStorage from 'vue-localstorage'

// Librería para realizar peticiones REST(endpoints)
import VueAxios from 'vue-axios'
import axios from 'axios'

import './assets/sass/main.scss'
import './assets/styles/main.css'
import './assets/theme/index.css'
import './assets/theme/stylesIconsFontastic.css'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale'
import lang from 'element-ui/lib/locale/lang/es'

// importando firebase
import '../node_modules/firebaseui/dist/firebaseui.css'
import firebase from 'firebase'
import { config } from './helpers/firebaseConfig'

import VueSocketIO from 'vue-socket.io'

Vue.use(VueLocalStorage)
Vue.use(VueAxios, axios)
Vue.use(ElementUI)


Vue.use(new VueSocketIO({
  debug: true,
  connection: 'http://localhost',
  vuex: {
      store,
      actionPrefix: 'SOCKET_',
      mutationPrefix: 'SOCKET_'
  },
  // options: { path: "/my-app/" } //Optional options
}))

// Configuración de idioma
Vue.use(ElementUI, { locale })
locale.use(lang)

export const HTTP = axios.create({
  baseURL: `http://localhost/api/`
  // baseURL: `http://10.40.80.131/api/`

  
  // baseURL: `https://griddata.herokuapp.com/`
  // baseURL: `http://10.20.30.110/api`
})
Vue.config.productionTip = false
firebase.initializeApp(config);

let app = '';

router.beforeEach((to,from,next)=>{
  let currentUser = firebase.auth().currentUser;
  const storageRutas = localStorage.getItem('rutas');
  const storageSubRutas = localStorage.getItem('subRutas');

  const rutas = storageRutas? JSON.parse(storageRutas):[];
  const subRutas = storageSubRutas? JSON.parse(storageSubRutas):[]; 

  let totalRutas = rutas.map(ruta=>ruta.url)
                  .concat(rutas.map(ruta=>ruta.children))
                  .concat(subRutas.map(ruta=>ruta.url))
                  .concat(subRutas.map(ruta=>ruta.children));

  totalRutas = totalRutas.join().split(',');
  if(!currentUser && to.fullPath!=='/'){next('/')}
  else if(currentUser && to.fullPath==='/asiento'){next()}
  else if(currentUser && rutas.length==0 && to.fullPath!=='/' && to.fullPath!=='/userNotAllowed'){next('/userNotAllowed')}
  else if(currentUser && to.fullPath==='/' && rutas.length>0){next(totalRutas[0])}
  else if(currentUser && rutas.length > 0 && !totalRutas.includes(to.fullPath)){next(false)}
  else {next()}
})



firebase.auth().onAuthStateChanged( async user=>{
  if(app===''){
    app = new Vue({  
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
  if(!user) localStorage.clear();
  const rutas = localStorage.getItem('rutas');
  if(rutas==null && user){
    const verifyUser = {email:user.email, userName:user.displayName};
    let res = await HTTP.post(`authenticate`, verifyUser)
    if(res.data.success){
      let rutaInicial = res.data.allowedRoutes.length>0? res.data.allowedRoutes[0].url:res.data.allowedSubRoutes[0].url;
      app.$store.state.rutas = res.data.allowedRoutes;
      app.$store.state.rutas = res.data.allowedSubRoutes;
      localStorage.setItem('rutas', JSON.stringify(res.data.allowedRoutes))
      localStorage.setItem('subRutas', JSON.stringify(res.data.allowedSubRoutes))
      app.$router.replace(rutaInicial);
    }else{
      localStorage.setItem('noaccess', 'noacceso')
      app.$router.push('/userNotAllowed')
    }
  }   
})



