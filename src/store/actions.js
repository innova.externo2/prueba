import ApiCall from '../core/http/index'

export default {
  async getData({ commit }, dataType) {
    ApiCall.getData(function(dataType, res) {
      commit(dataType, res)
    }, dataType)
  },
  async patchDataFilter({ commit }, payload) {
    let statusMutation = ''
    ApiCall.patchDataFilter(function(statusMutation, data) {
      commit(statusMutation, data)
    })
  },

  async getListGenders(){
    
  },

  async getDataFilters({ commit }, payload) {
    ApiCall.getDataFilters(function(payload, res) {
      commit(payload, res)
    }, payload)
  },

  changeStatus({ commit }, payload) {
    let statusMutation = ''
    ApiCall.changeAvailability(
      function(statusMutation, payload) {
        commit(statusMutation, payload)
      },
      payload,
      statusMutation
    )
  },
  setMaterial({ commit }, material) {
    commit('SET_MATERIAL', material)
  },
  async getDataById({ commit }, payload) {
    let statusMutation = ''
    ApiCall.individualData(
      function(statusMutation, data) {
        commit(statusMutation, data)
      },
      payload,
      statusMutation
    )
  },
  async patchData({ commit }, payload) {
    let statusMutation = ''
    ApiCall.updateData(
      function(statusMutation, data) {
        commit(statusMutation, data)
      },
      payload,
      statusMutation
    )
  },
  getGridDateProgram({ commit }, payload) {
    let statusMutation = ''
    ApiCall.weeklySchedule(
      function(statusMutation, data) {
        commit(statusMutation, data)
      },
      payload,
      statusMutation
    )
  },
  sendProgramObj({ commit }, info) {
    commit('SEND_PPROGRAM_OBJ', info)
  },
  SELECTED_PROGRAMS({ commit }, data) {
    commit('SELECTED_PROGRAMS', data)
  },
  async getInfo({ commit }, dataType) {
    const { route, name } = dataType
    ApiCall.getData(function(route, res) {
      commit(name, res.data)
    }, route)
  },
  filterWeekSelected({ commit }, week) {
    commit('weekSelected', week)
  },
  isMatOfGeminisValid({ commit }, data) {
    commit('isMatOfGeminis', data)
  },
  setItemInConfirmedContract({ commit }, data) {
    commit('setItemContract', data)
  },
  setContractInConfirmedContract({ commit }, data) {
    commit('setContractConfirmed', data)
  },
  setContract({ commit }, data) {
    commit('setContractInfo', data)
  },
  setContractStatus({ commit }, data) {
    commit('setContractStatusInfo', data)
  },
  setContractOriginal({ commit }, data) {
    commit('setContractOriginalInfo', data)
  },
  setContractIdReception({ commit }, data) {
    commit('setContractIdReceptionInfo', data)
  },
  setContractReception({ commit }, data) {
    commit('setContractReceptionInfo', data)
  },
  setContractReceived({ commit }, data) {
    commit('setContractReceivedInfo', data)
  },
  setIdStorageMat({ commit }, data) {
    commit('setIdStorageMatInfo', data)
  },
}
