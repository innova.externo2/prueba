import { stat } from "fs";

export default {
  allPrograms: (state) => state.programs,
  getProgramsFilters: (state) => state.programFilters,
  allSegments: (state) => state.segments,
  allContracts: (state) => state.contracts,
  allMaterials: (state) => state.materials,
  filterNumberMovement: (state) => state.filterMovement,
  getProgramsByChannel:
    (state) => (channel) => {
      const programs = state.allPrograms
      return programs.filter(program=>program.checkedChanels.length>0 
        && program.checkedChanels.filter(el=>el.toLowerCase() === channel.toLowerCase()).length>0)
    },
  guideClosure:
    (state) => (channel, daysOfTransmission) => {
      const daysClosed = state.guideClosure
      let daysClosedByChannel = daysClosed.filter(day => {
        if (channel === day.channel) {
          return day
        }
      })

      let arrOfDaysWithClosing = []
      let guideClosureOfDays = []
      daysClosedByChannel.map((day) => {
        if (daysOfTransmission.includes(day.closedDay)) {
          arrOfDaysWithClosing.push(day.closedDay)
          guideClosureOfDays.push(day)
        }
      })
      return {guideClosureOfDays, arrOfDaysWithClosing}
  },
  filterForWeek: (state) => (weekSelect) => {
    let coincidence = state.allGrid.filter((e) => e.week == weekSelect)
    if(coincidence.length >= 1) {
      coincidence
    } else {
      coincidence = "Week not found weekDate"
    }
    return coincidence
  },
  limitMovementTenRegisters: (state) => {
    let limit = state.movement.slice(0, 5)
    return limit
  },
  listOfCannedContentType: (state) => {
    console.log("listOfCannedContentType")
    const filterItems = state.typeMaterial.filter((e) =>  e.typeContent === 'Enlatado')
    return filterItems
  },
  listOfTelevisionContentType: (state) => {
    console.log("listOfTelevisionContentType")
    const filterItems = state.typeMaterial.filter((e) =>  e.typeContent === 'Televisa')
    return filterItems
  },
  
}
