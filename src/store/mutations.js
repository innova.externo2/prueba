import { stat } from "fs";

const order = data => {
  return data.sort(function (x, y) { return y._id - x._id || y.status - x.status })
}

export default {
  materials (state, data) {
    let materials = order(data.data.materials)
    state.materials = materials
  },
  Novelas (state, data) {
    let novels  = order(data.data.materials)
    state.Novelas = novels
  },
  Series (state, data) {
    let Series  = order(data.data.materials)
    state.Series = Series
  },
  Dibujos (state, data) {
    let Dibujos  = order(data.data.materials)
    state.Dibujos = Dibujos
  },
  Largometrajes (state, data) {
    let Largometrajes  = order(data.data.materials)
    state.Largometrajes = Largometrajes
  },
  Miniseries (state, data) {
    let Miniseries  = order(data.data.materials)
    state.Miniseries = Miniseries
  },
  Especial (state, data) {
    let Especial  = order(data.data.materials)
    state.Especial = Especial
  },
  Cocina(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Cocina = response
  },
  Entretenimiento(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Entretenimiento = response
  },
  Miniseries(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Cocina = response
  },
  Magazine(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Magazine = response
  },
  Deportivo(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Deportivo = response
  },
  Femenino(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Femenino = response
  },
  Periodístico(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Periodistico = response
  },
  Electoral(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Electoral = response
  },
  Entrevista(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Entrevista = response
  },
  Microprograma(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.Microprograma = response
  },
  Cómico(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.comico = response
  },
  ProgramEspecial(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.programEspecial = response
  },
  Religioso(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.religioso = response
  },
  Talkshow(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.talkshow = response
  },
  Político(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.politico = response
  },
  Infantil(state, data) {
    const {data: {materials}} = data
    let response = order(materials)
    state.infantil = response
  },
  'programs/allprograms/allprograms' (state, data) {
    let programs  = data.data.programs.sort(function (a, b) { if ( Number(a.geminisCode) < Number(b.geminisCode) ) {  return 1; } if ( Number(a.geminisCode) > Number(b.geminisCode )) {
    return -1; } return 0; });
    state.allPrograms = programs
  },
  'materials/allMaterials/allMaterials' (state, data) {
    let materials  = data.data.materials.sort(function (a, b) { if ( Number(a.geminisCode) < Number(b.geminisCode) ) {  return 1; } if ( Number(a.geminisCode) > Number(b.geminisCode )) {
    return -1; } return 0; });
    state.allMaterials = materials
  },
  programs (state, data) {
    let programs = data.data.programs.sort(function (x, y) { return y._id - x._id || y.status - x.status })
    state.programs = programs
  },
  segments (state, data) {
    let segments = data.data.segments.sort(function (x, y) { return y._id - x._id || y.status - x.status })
    state.segments = segments
  },
  amortization (state, data) {
    state.amortization = data.data
  },
  contracts (state, data) {
    state.contracts = data.data.contracts
  },
  movement (state, data) {
    state.movement = data.data
  },
  tables (state, data) {
    state.tables = data.data
  },
  supports (state, data) {
    state.supports = data.data
  },
  material (state, material) {
    state.material = material
  },
  program (state, program) {
    state.program = program
  },
  guideClosure (state, guideClosure) {
    state.guideClosure = guideClosure.data.guideClosure
  },
  storageSupport (state, data) {
    state.storageSupport = data.data.storageSupport
  },
  PATCH_MATERIAL (state, material) {
    state.material = material
  },
  STATUS_FALSE (state, payload) {
    payload.payloadInfo.status = false
    let dataType
    if (payload.type === 'materials') {
      dataType = state.materials
    } else if (payload.type === 'programs') {
      dataType = state.programs
    } else if (payload.type === 'segments') {
      dataType = state.segments
    }
    dataType.sort(function (x, y) { return y._id - x._id || y.status - x.status })
  },
  STATUS_TRUE (state, payload) {
    payload.payloadInfo.status = true
    let dataType
    if (payload.type === 'materials') {
      dataType = state.materials
    } else if (payload.type === 'programs') {
      dataType = state.programs
    } else if (payload.type === 'segments') {
      dataType = state.segments
    }
    dataType.sort(function (x, y) { return y._id - x._id || y.status - x.status })
  },
  SET_MATERIAL (state, material) {
    state.material = material
  },
  SET_GRID_PROGRAM (state, data) {
    state.gridProgramWeek = data
  },
  SEND_PPROGRAM_OBJ (state, info) {
    state.gridProgramObj = info
  },
  changeToAmortizationViewcontract (state, contract) {
    state.itemsChangeAmortz = contract
  },
  updateTextMaterialEnlatados(state, data) {
    state.textLoadingMaterialEnlatados = data
  },
  updateTextMaterial(state, data) {
    state.textLoadingMaterial = data
  },
  updateTexContracts(state, data) {
    state.textLoadingContracts = data
  },
  SELECTED_PROGRAMS (state, data) {
    state.programFilters = data
  },
  SEARCH_MOVEMENT (state, data) {
    if (data.type == 'numberMovement') {
      let selectMovement = state.movement.filter((el) => el.movementNumber == Number(data.textSearch))
      state.filterMovement = selectMovement
    }
    else {
      let fecha = new Date(data.textSearch)
      let month = fecha.getMonth()+1
      let year = fecha.getFullYear()
      let selectMovement = state.movement.filter((el) => Number(el.year) == year && Number(el.month) == month )
      console.log({month, year, selectMovement, state: state.movement}, 'store')
      state.filterMovement = selectMovement
    }
  },
  gridWeek(state, gridWeek) {
    state.gridWeek = gridWeek
  },
  grid(state, data) {
    state.allGrid = data.data.grid
  },
  weekSelected(state, data) {
    
    const selectWeek = state.allGrid.filter((e) => e.week === data.week)
    let dataSelect = []

    if (data.type == 'grid') {
      selectWeek.forEach((el) => {
        dataSelect = el.programs.filter((elem) => elem.refProgram && elem.refProgram.codProgram === data.idProgram)
      })
    }
    else {
      selectWeek.forEach((el) => {
        dataSelect = el.preliminaryPrograms.filter((elem) => elem.refProgram && elem.refProgram.codProgram === data.idProgram)
      })
    }

    let fullGridData, newObj
    if(selectWeek[0]) {
      const {_id = "There is no id", week = "Week not found", programs = [], preliminaryPrograms = [] } = selectWeek[0]
      
      if(_id == "There is no id") {
        fullGridData = []
      } else {
        fullGridData = {
          _id,
          programs,
          preliminaryPrograms,
          week
        }
      }
      newObj = {
        _id,
        count: dataSelect.length,
        fullGrid: fullGridData,
        gridMatch: dataSelect,
        weekDate: week
  
      }
    } else {
      const obj =  {
        count: 0,
        fullGrid: Array(0),
        gridMatch: Array(0),
        weekDate: "Week not found",
        _id: "There is no id"
      }
      newObj = obj
    }

    console.log(newObj, 'newObj*****')
    
    state.weekSelect = newObj
  },
  isMatOfGeminis(state, data) {
    state.isMatSelectOfGeminis = data
  },
  setItemContract(state, data) {
    state.setItem = data
  },
  setContractConfirmed(state, data) {
    state.setContract = data
  },
  setContractInfo(state, data) {
    state.setInContract = data
  },
  setContractStatusInfo(state, data) {
    state.contractStatus = data
  },
  setContractOriginalInfo(state, data) {
    state.contractOriginal = data
  },
  setContractIdReceptionInfo(state, data) {
    state.idContractRecp = data
  },
  setContractReceptionInfo(state, data) {
    state.contractRecp = data
  },
  setContractReceivedInfo(state, data) {
    state.contractReceived = data
  },
  setIdStorageMatInfo(state, data) {
    state.idStorageMat = data
  },
  setContractForStorageCanned(state, data) {
    state.contractForStorageCanned = data
  },
  addOrUpdateAllGrid(state,data){
    let gridFound = state.allGrid.find(grid=>data._id==grid._id);
    if(!gridFound){
      state.allGrid.push(data);
    }
    else{
      const index = state.allGrid.findIndex(el=>el._id===data._id);
      state.allGrid[index] = data;
    } 
  },
  addOrUpdateAllPrograms(state,data){
    let programFound = state.allPrograms.find(program=>program._id==data._id);
    if(!programFound) state.allPrograms.push(data);
    else{
      const index = state.allPrograms.findIndex(el=>el._id===data._id)
      state.allPrograms[index] = data;
    }
  },
  typeMaterial(state, data) {
    state.typeMaterial = data.data.response
  },
}
