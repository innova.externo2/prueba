/* eslint-disable no-unused-vars */
import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

// Vistas en común
import Login from '../views/Login.vue'
import Review from '../views/review/Review.vue'
import MaterialesEnlatados from '../views/review/MaterialesEnlatados.vue'
import revListaAfiliados from '../views/review/revListaAfiliados.vue'
import ProduccionNacional from '../views/review/ProduccionNacional.vue'
import FusionGourmet from '../views/review/fusion-gourmet/fusionGourmet.vue'
import Movies from '../views/review/movies/Movies.vue'
import Novels from '../views/review/novels/Novels.vue'
import formByCap from '../views/review/novels/formByCap.vue'
import Cartoons from '../views/review/cartoons/Cartoons.vue'
import Specials from '../views/review/specials/specials.vue'
import Miniseries from '../views/review/miniseries/miniseries.vue'
import Revision from '../views/review/revision.vue'
import RevisionNacional from '../views/review/revisionNacional.vue'
import RevNovel from '../views/review/novels/RevNovel.vue'
import RevNovelDoc from '../views/review/novels/RevNovelDoc.vue'
import RevChapNovel from '../views/review/RevChapNovel.vue'
import RevChapNovel2 from '../views/review/RevChapNovel2.vue'
import reportCap from '../views/review/novels/reportCap.vue'
import RevSeriesDoc from '../views/review/series/RevSeriesDoc.vue'
import RevChapSeries from '../views/review/RevChapSeries.vue'
import repositionMaterialsWithCap from '../views/review/novels/repositionMaterialsWithCap.vue'


import Report from '../views/review/report.vue'
import ReporteNacional from '../views/review/reportNacional.vue'
import Reposition from '../views/review/reposition.vue'
import RevFusionGourmet from '../views/review/fusion-gourmet/RevFusionGourmet.vue'
import ReportFusionGourmet from '../views/review/fusion-gourmet/ReportFusionGourmet.vue'

import TypeMaterial from '../views/review/typeMaterial/index.vue'
/*Afiliados */
import revAfiliadoTelevisa from '../views/review/afiliados/revAfiliadoTelevisa.vue'
import TipoDeMaterial from '../views/review/afiliados/TipoDeMaterial.vue'

// Vistas de María Helena
import Material from '../views/materials/Material.vue'
import Programs from '../views/programs/Programs.vue'
import Segments from '../views/segments/Segments.vue'
import Grid from '../views/grid/index.vue'
import GridContent from '../views/grid/gridContent.vue'
import ProgramGrid from '../views/grid/ProgramGrid.vue'
import Views from '../views/Views.vue'
import Cut from '../views/materials/Cut.vue'
import NewMaterial from '../views/materials/NewMaterial.vue'
import NewProgram from '../views/programs/NewProgram.vue'
import NewSegment from '../views/segments/NewSegment.vue'
import EditProgram from '../views/programs/EditProgram.vue'
import EditSegment from '../views/segments/EditSegment.vue'

import EditCuts from '../views/materials/EditCuts.vue'
import ContractsProvider from '../views/review/ContractsProvider.vue'

// Vistas de Ana Sofía
import Material2 from '../views/materials/Material2.vue'
import Contract from '../views/contract/Contract.vue'
import Amortization from '../views/amortization/Amortization'
import Support from '../views/support/Support.vue'
import NewMaterial2 from '../views/materials/NewMaterial2.vue'
import NewContract from '../views/contract/NewContract.vue'
import NewAmortization from '../views/amortization/NewAmortization.vue'
import EditAmortization from '../views/amortization/EditAmortization.vue'
import AmortizationContract from '../views/amortization/AmortizationContract.vue'
import ConfirmedContract from '@/views/contract/ConfirmedContract.vue'
import Asiento from '../views/sits/SitGenerate.vue' //goToGenerate

// VISTAS DE RENATO
import RecordOfDiscs from '../views/support/recordOfDiscs/RecordOfDiscs.vue'
import NationalProduction from '../views/support/nationalProduction/NationalProduction.vue'
import CannedMaterials from '../views/support/cannedMaterials/CannedMaterials.vue'
import SupportNewOrder from '../views/support/recordOfDiscs/SupportNewOrder.vue'
import SupportHistory from '../views/support/recordOfDiscs/SupportHistory.vue'
import History from '../views/support/recordOfDiscs/History.vue'
import StorageCannedMat from '../views/support/cannedMaterials/StorageCannedMat.vue'
import StorageCanned from '../views/support/cannedMaterials/StorageCanned.vue'
import CheckReception from '../views/support/cannedMaterials/CheckReception.vue'
import Reception from '../views/support/cannedMaterials/Reception.vue'
import Fusion from '../views/support/fusionGourmet/Fusion.vue'
import RecordCanned from '../views/support/cannedMaterials/RecordCanned.vue'
import HistoryContract from '../views/contract/HistoryContract.vue'
import OrdersPlaced from '../views/support/recordOfDiscs/OrdersPlaced.vue'
import ReceptionRecord from '../views/support/recordOfDiscs/ReceptionRecord.vue'
import ReceptionMaterialsNac from '../views/support/nationalProduction/ReceptionMaterialsNac.vue'
import HistoryRecordCanned from '../views/support/cannedMaterials/HistoryRecordCanned.vue'
import FormProductNac from '../views/support/nationalProduction/FormProductNac.vue'
import ReceptionNational from '../views/support/nationalProduction/receptionMaterialNac2.vue'
import ReportNational from '../views/support/nationalProduction/reportMaterialNac2.vue'
import DiskHistory from '../views/support/recordOfDiscs/DiskHistory.vue'
import ListOfMaterialsOnDisk from '../views/support/recordOfDiscs/ListOfMaterialsOnDisk.vue'

import Afiliaciones from '../views/support/Afiliaciones/Afiliaciones.vue'
import Televisa from '../views/support/Afiliaciones/Televisa/Televisa.vue'
import TypeMaterials from '../views/support/Afiliaciones/Televisa/TypeMaterials.vue'
import TipoDeMaterialNacional from '../views/support/Afiliaciones/Televisa/tipoMaterialNacional.vue'
import ReceptionTelevisa from '../views/support/Afiliaciones/Televisa/ReceptionTelevisa.vue'
import formTelevisa from '../views/support/Afiliaciones/Televisa/formTelevisa.vue'
import AlmacenTelevisa from '../views/support/Afiliaciones/Televisa/AlmacenTelevisa.vue'
import HistorialTelevisa from '../views/support/Afiliaciones/Televisa/HistorialTelevisa.vue'
import TablaTelevisa from '../views/support/Afiliaciones/Televisa/TablaTelevisa.vue'
import FormAlmacenTelevisa from '../views/support/Afiliaciones/Televisa/FormAlmacenTelevisa.vue'
import RecepcionSinCap from '../views/support/Afiliaciones/Televisa/RecepcionSinCap.vue'
import AlmacenSinCap from '../views/support/Afiliaciones/Televisa/AlmacenSinCap.vue'
import fusionGourmetSoporte from '../views/support/nationalProduction/fusionGourmetSoporte.vue'

import TypeFusion from '../views/support/TypeFusion.vue'
import TiposFusionRevision from '../views/review/fusion-gourmet/TiposFusionRevision.vue'

// PARRILLA
import parrillaIntranet from '../views/grid/grids-tables/grid-intranet.vue'
import parrillaJefatura from '../views/grid/grids-tables/grid-head.vue'
import parrillaExtranjero from '../views/grid/grids-tables/grid-foreign.vue'
import parrillaPromociones from '../views/grid/grids-tables/grid-promotions.vue'
import parrillaInterno from '../views/grid/grids-tables/grid-internal.vue'
import viewPrint from '../views/grid/grids-tables/viewPrint.vue'
import viewPrintGeneral from '../views/grid/grids-tables/viewPrintGeneral.vue'
import viewPrintPrice from '../views/grid/grids-tables/viewPrintPrice.vue'
import viewPrintJefatura from '../views/grid/grids-tables/viewPrintJefatura.vue'
import closeGuide from '../views/grid/closeGuide.vue'
import parrillaPrecios from '../views/grid/grids-tables/parrillaPrecios.vue'

// USUARIO NO PERMITIDO
import userNotAllowed from '../views/userNotAllowed.vue'
import PageNotFound from '../views/PageNotFound.vue'

// MOVIMIENTOS
import movement from '../views/movement/movement'
import printMovement from '../views/movement/printMovement'

// PARRILLA PRELIMINAR
import gridContentPreliminary from '../views/grid/preliminaryGrill/gridContentPreliminary'
import programGridPreliminary from '../views/grid/preliminaryGrill/programGridPreliminary'
import gridPreliminary from '../views/grid/preliminaryGrill/gridPreliminary'

//ADMINISTRAR PERMISOS
import managePermissions from '../views/permissions/ManagePermissions'



Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/material',
      name: 'material',
      component: Material,
      meta: {
        autentificado: true
      }
    },
    {
      path: '/material2',
      name: 'material2',
      component: Material2
    },
    {
      path: '/programs',
      name: 'programs',
      component: Programs
    },
    {
      path: '/segments',
      name: 'segments',
      component: Segments
    },
    {
      path: '/review',
      name: 'review',
      component: Review
    },
    {
      path: '/materialesEnlatados',
      name: 'materialesEnlatados',
      component: MaterialesEnlatados
    },
    {
      path: '/produccionNacional',
      name: 'produccionNacional',
      component: ProduccionNacional
    },
    {
      path: '/revListaAfiliados',
      name: 'revListaAfiliados',
      component: revListaAfiliados
    },
    {
      path: '/revAfiliadoTelevisa',
      name: 'revAfiliadoTelevisa',
      component: revAfiliadoTelevisa
    },
    {
      path: '/fusionGourmet',
      name: 'fusionGourmet',
      component: FusionGourmet
    },
    {
      path: '/grid',
      name: 'grid',
      component: Grid
    },
    {
      path: '/gridContent',
      name: 'gridContent',
      component: GridContent
    },
    {
      path: '/views',
      name: 'views',
      component: Views
    },
    {
      path: '/contract',
      name: 'contract',
      component: Contract
    },
    {
      path: '/amortization',
      name: 'amortization',
      component: Amortization
    },
    {
      path: '/support',
      name: 'support',
      component: Support
    },
    {
      path: '/newMaterial',
      name: 'newMaterial',
      component: NewMaterial
    },
    {
      path: '/newMaterial2',
      name: 'newMaterial2',
      component: NewMaterial2
    },
    {
      path: '/newProgram',
      name: 'newProgram',
      component: NewProgram
    },
    {
      path: '/newSegment',
      name: 'newSegment',
      component: NewSegment
    },
    {
      path: '/newContract',
      name: 'newContract',
      component: NewContract
    },
    {
      path: '/newAmortization',
      name: 'newAmortization',
      component: NewAmortization
    },
    {
      path: '/editProgram',
      name: 'editProgram',
      component: EditProgram
    },
    {
      path: '/editSegment',
      name: 'editSegment',
      component: EditSegment
    },
    {
      path: '/editCuts',
      name: 'editCuts',
      component: EditCuts
    },
    {
      path: '/editAmortization',
      name: 'editAmortization',
      component: EditAmortization
    },
    {
      path: '/cut',
      name: 'cut',
      component: Cut
    },
    {
      path: '/programGrid',
      name: 'programGrid',
      component: ProgramGrid
    },
    {
      path: '/revChapNovel',
      name: 'revChapNovel',
      component: RevChapNovel
    },
    {
      path: '/revChapNovel2',
      name: 'revChapNovel2',
      component: RevChapNovel2
    },
    {
      path: '/revChapSeries',
      name: 'revChapSeries',
      component: RevChapSeries
    },
    {
      path: '/amortizationContract',
      name: 'amortizationContract',
      component: AmortizationContract
    },
    {
      path: '/confirmedContract',
      name: 'confirmedContract',
      component: ConfirmedContract
    },
    {
      path: '/recordOfDiscs',
      name: 'recordOfDiscs',
      component: RecordOfDiscs
    },
    {
      path: '/nationalProduction',
      name: 'nationalProduction',
      component: NationalProduction
    },
    {
      path: '/cannedMaterials',
      name: 'cannedMaterials',
      component: CannedMaterials
    },
    {
      path: '/supportNewOrder',
      name: 'supportNewOrder',
      component: SupportNewOrder
    },
    {
      path: '/supportHistory',
      name: 'supportHistory',
      component: SupportHistory
    },
    {
      path: '/history',
      name: 'history',
      component: History
    },
    {
      path: '/storageCanned',
      name: 'storageCanned',
      component: StorageCanned
    },
    {
      path: '/storageCannedMat',
      name: 'storageCannedMat',
      component: StorageCannedMat
    },
    {
      path: '/reception',
      name: 'reception',
      component: Reception
    },
    {
      path: '/checkReception',
      name: 'checkReception',
      component: CheckReception
    },
    {
      path: '/recordCanned',
      name: 'recordCanned',
      component: RecordCanned
    },
    {
      path: '/historyContract',
      name: 'historyContract',
      component: HistoryContract
    },
    {
      path: '/ordersPlaced',
      name: 'ordersPlaced',
      component: OrdersPlaced
    },
    {
      path: '/fusion',
      name: 'fusion',
      component: Fusion
    },
    {
      path: '/userNotAllowed',
      name: 'userNotAllowed',
      component: userNotAllowed
    },
    {
      path: '/pageNotFound',
      name: 'pageNotFound',
      component: PageNotFound
    },
    {
      path: '/historyRecordCanned',
      name: 'historyRecordCanned',
      component: HistoryRecordCanned
    },
    {
      path: '/diskHistory',
      name: 'diskHistory',
      component: DiskHistory
    },
    {
      path: '/listOfMaterialsOnDisk',
      name: 'listOfMaterialsOnDisk',
      component: ListOfMaterialsOnDisk
    },
    /* Revision Routes */
    {
      path: '/largometrajes',
      name: 'largometrajes',
      component: Movies
    },
    {
      path: '/report',
      name: 'report',
      component: Report
    },
    {
      path: '/reporteNacional',
      name: 'reporteNacional',
      component: ReporteNacional
    },
    {
      path: '/revision',
      name: 'revision',
      component: Revision
    },
    {
      path: '/revisionNacional',
      name: 'revisionNacional',
      component: RevisionNacional
    },
    {
      path: '/reposition',
      name: 'reposition',
      component: Reposition
    },
    {
      path: '/novels',
      name: 'novels',
      component: Novels
    },
    {
      path: '/revNovel',
      name: 'revNovel',
      component: RevNovel
    },
    {
      path: '/revNovelDoc',
      name: 'revNovelDoc',
      component: RevNovelDoc
    },
    {
      path: '/formByCap',
      name: 'formByCap',
      component: formByCap
    },
    {
      path: '/reportCap',
      name: 'reportCap',
      component: reportCap
    },
    {
      path: '/revSeriesDoc',
      name: 'revSeriesDoc',
      component: RevSeriesDoc
    },
    {
      path: '/cartoons',
      name: 'cartoons',
      component: Cartoons
    },
    {
      path: '/repositionMaterialsWithCap',
      name: 'repositionMaterialsWithCap',
      component: repositionMaterialsWithCap
    },
    {
      path: '/miniseries',
      name: 'miniseries',
      component: Miniseries
    },
    {
      path: '/specials',
      name: 'specials',
      component: Specials
    },
    {
      path: '/revFusionGourmet',
      name: 'revFusionGourmet',
      component: RevFusionGourmet
    },
    {
      path: '/reportFusionGourmet',
      name: 'reportFusionGourmet',
      component: ReportFusionGourmet
    },
    {
      path: '/receptionRecord',
      name: 'receptionRecord',
      component: ReceptionRecord
    },
    {
      path: '/receptionMaterialsNac',
      name: 'receptionMaterialsNac',
      component: ReceptionMaterialsNac
    },
    {
      path: '/formProductNac',
      name: 'formProductNac',
      component: FormProductNac
    },
    {
      path: '/receptionNational',
      name: 'receptionNational',
      component: ReceptionNational
    },
    {
      path: '/reportNational',
      name: 'reportNational',
      component: ReportNational
    },
    {
      path: '/afiliaciones',
      name: 'afiliaciones',
      component: Afiliaciones
    },
    {
      path: '/Televisa',
      name: 'Televisa',
      component: Televisa
    },
    {
      path: '/typeMaterials',
      name: 'typeMaterials',
      component: TypeMaterials
    },
    {
      path: '/tipoDeMaterialNacional',
      name: 'tipoDeMaterialNacional',
      component: TipoDeMaterialNacional
    },
    {
      path: '/receptionTelevisa',
      name: 'receptionTelevisa',
      component: ReceptionTelevisa
    },
    {
      path: '/formTelevisa',
      name: 'formTelevisa',
      component: formTelevisa
    },
    {
      path: '/tiposFusionRevision',
      name: 'tiposFusionRevision',
      component: TiposFusionRevision
    },
    {
      path: '/typeFusion',
      name: 'typeFusion',
      component: TypeFusion
    },
    {
      path: '/almacenTelevisa',
      name: 'almacenTelevisa',
      component: AlmacenTelevisa
    },
    {
      path: '/formAlmacenTelevisa',
      name: 'formAlmacenTelevisa',
      component: FormAlmacenTelevisa
    },
    {
      path: '/tablaTelevisa',
      name: 'tablaTelevisa',
      component: TablaTelevisa
    },
    {
      path: '/historialTelevisa',
      name: 'historialTelevisa',
      component: HistorialTelevisa
    },
    {
      path: '/recepcionSinCap',
      name: 'recepcionSinCap',
      component: RecepcionSinCap
    },
    {
      path: '/almacenSinCap',
      name: 'almacenSinCap',
      component: AlmacenSinCap
    },
    {
      path: '/contractsProvider',
      name: 'contractsProvider',
      component: ContractsProvider
    },
    {
      path: '/typeMaterial',
      name: 'typeMaterial',
      component: TypeMaterial
    },
    {
      path: '/tipoDeMaterial',
      name: 'tipoDeMaterial',
      component: TipoDeMaterial
    },
    {
      path: '/fusionGourmetSoporte',
      name: 'fusionGourmetSoporte',
      component: fusionGourmetSoporte
    },
    {
      path: '/parrillaJefatura',
      name: 'parrillaJefatura',
      component: parrillaJefatura
    },
    {
      path: '/parrillaExtranjero',
      name: 'parrillaExtranjero',
      component: parrillaExtranjero
    },
    {
      path: '/parrillaPromociones',
      name: 'parrillaPromociones',
      component: parrillaPromociones
    },
    {
      path: '/parrillaIntranet',
      name: 'parrillaIntranet',
      component: parrillaIntranet
    },
    {
      path: '/parrillaInterno',
      name: 'parrillaInterno',
      component: parrillaInterno
    },
    {
      path: '/viewPrint',
      name: 'viewPrint',
      component: viewPrint
    },
    {
      path: '/viewPrintGeneral',
      name: 'viewPrintGeneral',
      component: viewPrintGeneral
    },
    {
      path: '/closeGuide',
      name: 'closeGuide',
      component: closeGuide
    },
    {
      path: '/parrillaPrecios',
      name: 'parrillaPrecios',
      component: parrillaPrecios
    },
    {
      path: '/viewPrintPrice',
      name: 'viewPrintPrice',
      component: viewPrintPrice
    },
    {
      path: '/viewPrintJefatura',
      name: 'viewPrintJefatura',
      component: viewPrintJefatura
    },
    {
      path: '/movement',
      name: 'movement',
      component: movement
    },
    {
      path: '/printMovement',
      name: 'printMovement',
      component: printMovement
    },
    {
      path: '/gridContentPreliminary',
      name: 'gridContentPreliminary',
      component: gridContentPreliminary
    },
    {
      path: '/programGridPreliminary',
      name: 'programGridPreliminary',
      component: programGridPreliminary
    },
    {
      path: '/gridPreliminary',
      name: 'gridPreliminary',
      component: gridPreliminary
    },
    {
      path: '/managePermissions',
      name: 'managePermissions',
      component: managePermissions
    },
    {
      path: '/asiento',
      name: 'asiento',
      component: Asiento
    },
  ]
})

export default router
