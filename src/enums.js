export const contentCardsData = [
    {name: 'TELEVISA - TELENOVELAS', colorTitle: 'color-darkblue', colorCaret: 'color-darkmagenta', routeImg: 'icono-político.svg', styleCard: 'border-card-politico', isBigLetters: false, instance: 'televisaNovel', isShow: false },
    {name: 'TELEVISA - SERIES', colorTitle: 'color-darkblue', colorCaret: 'color-darkmagenta', routeImg: 'icono-político.svg', styleCard: 'border-card-politico', isBigLetters: false, instance: 'televisaSerie', isShow: false},
    {name: 'TELEVISA - ESPECIALES', colorTitle: 'color-darkblue', colorCaret: 'color-darkmagenta', routeImg: 'icono-político.svg', styleCard: 'border-card-politico', isBigLetters: false, instance: 'televisaSpecial', isShow: false},
    {name: 'Político', colorTitle: 'color-darkblue', colorCaret: 'color-darkmagenta', routeImg: 'icono-político.svg', styleCard: 'border-card-politico', isBigLetters: false, instance: 'politicalCounter', isShow: true},
    {name: 'Entretenimiento', colorTitle: 'color-pink-card', colorCaret: 'color-darkblue', routeImg: 'icono-entretenimiento.svg', styleCard: 'bg-pinkCard', isBigLetters: false, instance: 'entertainmentCounter', isShow: true},
    {name: 'Religioso', colorTitle: 'color-aqua', colorCaret: 'color-card-child', routeImg: 'icono-religioso.svg', styleCard: 'border-card-religious', isBigLetters: false, instance: 'religiousCounter', isShow: true},
    {name: 'Electoral', colorTitle: 'color-darkred', colorCaret: 'color-aqua', routeImg: 'icono-electoral.svg', styleCard: 'border-card-electoral', isBigLetters: false, instance: 'electoralCounter', isShow: true},
    {name: 'Largometrajes', colorTitle: 'color-blue', colorCaret: 'color-lightblue', routeImg: 'largometraje.svg', styleCard: 'border-card-movie', isBigLetters: false, instance: 'movCount', isShow: true},
    {name: 'Novelas', colorTitle: 'color-pink', colorCaret: 'color-blue', routeImg: 'novela.svg', styleCard: 'border-card-novels', isBigLetters: false, instance: 'novelsCount', isShow: true},
    {name: 'Series', colorTitle: 'color-lightblue', colorCaret: 'color-pink', routeImg: 'serie.svg', styleCard: 'border-card-series', isBigLetters: false, instance: 'seriesCount', isShow: true},
    {name: 'Femenino', colorTitle: 'color-darkpurple', colorCaret: 'color-pink-card', routeImg: 'icono-femenino.svg', styleCard: 'border-card-femenino', isBigLetters: false, instance: 'femaleCounter', isShow: true},
    {name: 'Infantil', colorTitle: 'color-card-child', colorCaret: 'color-purple', routeImg: 'icono-infantil.svg', styleCard: 'border-card-child', isBigLetters: false, instance: 'childCounter', isShow: true},
    {name: 'Magazine', colorTitle: 'color-green', colorCaret: 'color-card-child', routeImg: 'icono-magazine.svg', styleCard: 'border-card-magazine', isBigLetters: false, instance: 'magazineCounter', isShow: true},
    {name: 'Entrevista', colorTitle: 'color-magenta', colorCaret: 'color-darkpink', routeImg: 'icono-entrevista.svg', styleCard: 'border-card-interview', isBigLetters: false, instance: 'Entrevista', isShow: true},
    {name: 'Miniseries', colorTitle: 'color-purple', colorCaret: 'color-yellow', routeImg: 'miniserie.svg', styleCard: 'border-card-miniseries', isBigLetters: false, instance: 'minCount', isShow: true},
    {name: 'Dibujos', colorTitle: 'color-yellow', colorCaret: 'color-lightgreen', routeImg: 'dibujo.svg', styleCard: 'border-card-cartoons', isBigLetters: false, instance: 'cartoonsCount', isShow: true},
    {name: 'Especial', colorTitle: 'color-lightgreen', colorCaret: 'color-purple', routeImg: 'especiales.svg', styleCard: 'border-card-specials', isBigLetters: false, instance: 'specialsCount', isShow: true},
    {name: 'Periodístico', colorTitle: 'color-darkmagenta', colorCaret: 'color-gray', routeImg: 'icono-periodístico.svg', styleCard: 'border-card-periodistico', isBigLetters: false, instance: 'periodisticoCounter', isShow: true},
    {name: 'Cómico', colorTitle: 'color-lightpurple', colorCaret: 'color-purple', routeImg: 'icono-cómico.svg', styleCard: 'bg-lightpurple' , isBigLetters: false, instance: 'comicCounter', isShow: true},
    {name: 'Talkshow', colorTitle: 'color-darkpink', colorCaret: 'color-magenta', routeImg: 'icono-talkshow.svg', styleCard: 'border-card-talkshow', isBigLetters: false, instance: 'talkshowCounter', isShow: true},
    {name: 'Microprograma', colorTitle: 'color-lightgray', colorCaret: 'color-darkpink', routeImg: 'icono-microprograma.svg', styleCard: 'border-card-microprograma', isBigLetters: false, instance: 'microprogramCounter', isShow: true},
    {name: 'Deportivo', colorTitle: 'color_light_blue', colorCaret: 'color-lightpurple', routeImg: 'icono-deportivo.svg', styleCard: 'bg-lightblue', isBigLetters: false, instance: 'deportCounter', isShow: true},
    {name: 'Cocina', colorTitle: 'color-gold', colorCaret: 'color-gray', routeImg: 'r_fusion.svg', styleCard: 'border-card-cook', isBigLetters: false, instance: 'cookCounter', isShow: true},
    {name: 'Programas Especiales', colorTitle: 'color-greenApple', colorCaret: 'color-gold', routeImg: 'icono-progespeciales.svg', styleCard: 'border-card-prog-especial', isBigLetters: true, instance: 'programEspecialCounter', isShow: true},
  ]

  export const contentTypeSupport = [
    {value:"XD CAM 50 - GB", label:"XD CAM 50 - GB"},
    {value:"XD CAM 23 - GB", label:"XD CAM 23 - GB"}
  ]
// 'Septiembre', 
 export const months = [
    'Enero', 
    'Febrero', 
    'Marzo', 
    'Abril', 
    'Mayo', 
    'Junio', 
    'Julio', 
    'Agosto', 
    'Septiembre', 
    'Octubre', 
    'Noviembre', 
    'Diciembre'
  ];

  export const typeOfContent = [
    {
      value: '300', //No definido
      label: 'Cocina'
    },
    {
      value: '070',
      label: 'Cómico'
    },
        {
      value: '111',
      label: 'Deportivo'
    },
        {
      value: '080',
      label: 'Dibujos'
    },
        {
      value: '300',//No definido
      label: 'Electoral'
    },
      {
      value: '090',
      label: 'Entretenimiento'
    },
      {
      value: '300',//No definido
      label: 'Entrevista'
    },
      {
      value: '060',
      label: 'Especial'
    },
      {
      value: '300',//No definido
      label: 'Femenino'
    },
      {
      value: '080',
      label: 'Infantil'
    },
      {
      value: '300',//No definido
      label: 'Magazine'
    },
      {
      value: '300',//No definido
      label: 'Microprograma'
    },
      {
      value: '040',
      label: 'Novelas'
    },
    {
      value: '010',
      label: 'Largometrajes'
    },
      {
      value: '300',//No definido
      label: 'Periodístico'
    },
      {
      value: '300',//No definido
      label: 'Político'
    },
    {
      value: '300',//No definido
      label: 'Religioso'
    },
      {
      value: '020',
      label: 'Series'
    },
      {
      value: '025',
      label: 'Miniseries'
    },
      {
      value: '060',
      label: 'Programas Especiales'
    },
    {
      value: '300',//No definido
      label: 'Talkshow'
    },
  ]


  export const dayNames = [
    'Lunes', 
    'Martes', 
    'Miércoles', 
    'Jueves', 
    'Viernes', 
    'Sábado', 
    'Domingo'];